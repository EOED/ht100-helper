# HT100 helper

This is meant to help users of the HT100 HACCP touch device. 

https://www.comarkinstruments.net/product/haccp-touch/#probe-selection

This is to help cooks of small business comply with food safety procedures. 

https://www.food.gov.uk/business-guidance/safer-food-better-business-sfbb

The development will be done in an online binder notebook to get some help. 

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/EOED%2Fht100-helper/6cc29b5abac6cbec87c712a0039e21c87098445e?filepath=HT100-helper.ipynb)


